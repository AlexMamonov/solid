                            //Wrong

interface IPlaystation
{
	int ChooseGame();
    BuyGame SetGame(int numOfToplist);
	float FindPrice(PriceList priceList, BuyGame buyGame);
	bool GoToShop(BuyGame buyGame);
    bool Play (BuyGame buyGame);  //Играем в купленную игру
	bool MultiPlayer(Game Destiny);
}


                                        //Right

interface IPlaystationGame
{
	bool ProcessOrder(BuyGame buyGame);
    bool Cook (Order order);  //Играем в купленную игру
}

interface IPlayMultiPlayer
{
	bool MultiPlayer(Game Destiny);
}

interface IFindPriseAndBuyGame
{
	float FindPrice(PriceList priceList, BuyGame buyGame);
	bool GoToShop(BuyGame buyGame);
}



